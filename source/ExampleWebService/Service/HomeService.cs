﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleWebService.Service
{
    public class HomeService
    {
        private int x;
        private int y;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return x; }  // Noncompliant: field 'y' is not used in the return value
            set { x = value; } // Noncompliant: field 'y' is not updated
        }

        public bool Bug()
        {
            /* Getters and setters should access the expected fields
             * 
             * Properties provide a way to enforce encapsulation by providing public, protected or 
             * internal methods that give controlled access to private fields. However in classes 
             * with multiple fields it is not unusual that cut and paste is used to quickly create 
             * the needed properties, which can result in the wrong field being accessed by a getter or setter.
             * 
             * This rule raises an issue in any of these cases:
             * 
             * A setter does not update the field with the corresponding name.
             * A getter does not access the field with the corresponding name.
             * For simple properties it is better to use auto-implemented properties (C# 3.0 or later).
             */

            var val = Y;
            return true;
        }

        public bool CodeSmell()
        {
            /* "is" should not be used with "this"
             * 
             * There's no valid reason to test this with is. The only plausible explanation for such a test 
             * is that you're executing code in a parent class conditionally based on the kind of child class 
             * this is. But code that's specific to a child class should be in that child class, not in the parent.
             */

            if (this is HomeService) // Noncompliant
            {
                return true;
            }
            return false;
        }

        public bool Vulnerability()
        {
            /* Generic exceptions should not be ignored
             * 
             * When exceptions occur, it is usually a bad idea to simply ignore them. Instead, 
             * it is better to handle them properly, or at least to log them.
             * 
             * This rule only reports on empty catch clauses that catch generic Exceptions.
             */
            try
            {
                DoSomething();
            }
            catch (Exception) { }

            return true;
        }

        public int DoSomething()
        {
            var val = 15;

            List<string> vals = new List<string>();
            for (int i = 0; i <= val; i++)
            {
                vals.Add($"The value is {i}");
            }
            var count = 0;
            foreach (var v in vals)
                if (Int32.Parse(v.Split(" ")[3]) % 2 == 0)
                    count++;

            return count;
        }

        public int DoSomethingAgain()
        {
            var val = 20;

            List<string> vals = new List<string>();
            for (int i = 0; i <= val; i++)
            {
                vals.Add($"The value is {i}");
            }
            var count = 0;
            foreach (var v in vals)
                if (Int32.Parse(v.Split(" ")[3]) % 2 == 0)
                    count++;

            return count;
        }
    }
}
