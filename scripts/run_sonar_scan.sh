#!/bin/bash

WORKINGDIR=$(dirname $0)

if [ -z "$1" ]; then
   echo "FAILED - INVALID PARAMETERS"
   exit 1
fi
SONAR_TOKEN=$1

if [ -z "$2" ]; then
   echo "FAILED - INVALID PARAMETERS"
   exit 1
fi
PROJECT=$2


echo "::: CURRENT DIR"
cd ./source
pwd
ls

echo "::: INSTALL DOTNET-SONARSCANNER"
dotnet tool install --global dotnet-sonarscanner

echo "::: SONARSCANNER BEGIN"
dotnet sonarscanner begin /k:"ruanreynekepersonalteam_dotnetsonarcloud" /d:sonar.organization="ruanreynekepersonalteam" /d:sonar.host.url="https://sonarcloud.io" /d:sonar.login="0038cd94758c41192188974c102b2fb499f9a244"

echo "::: DOTNET BUILD"
dotnet build

echo "::: CURRENT DIR"
find / -name "sonarqube"

echo "::: SONARSCANNER END"
dotnet sonarscanner end /d:sonar.login="0038cd94758c41192188974c102b2fb499f9a244"