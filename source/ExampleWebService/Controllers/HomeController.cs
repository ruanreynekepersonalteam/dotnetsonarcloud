﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExampleWebService.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ExampleWebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private static int usageCount = 0;

        [HttpGet]
        public ActionResult<string> Get()
        {
            usageCount++;
            return "Welcome to the Example Web Service!";
        }


        [HttpGet("Usage")]
        public ActionResult<string> GetUsage()
        {
            usageCount++;
            return $"The current count is {usageCount}.";
        }


        [HttpGet("Now")]
        public ActionResult<string> GetDateAndTime()
        {
            usageCount++;
            return $"The current date and time is {DateTime.Now}.";
        }


        [HttpGet("Bug")]
        public ActionResult<string> GetBug()
        {
            usageCount++;
            var service = new HomeService();
            return $"Executing Bug: {service.Bug()}";
        }

        [HttpGet("Vulnerability")]
        public ActionResult<string> GetVulnerability()
        {
            usageCount++;
            var service = new HomeService();
            return $"Executing Vulnerability: {service.Vulnerability()}";
        }

        [HttpGet("CodeSmell")]
        public ActionResult<string> GetCodeSmell()
        {
            usageCount++;
            var service = new HomeService();
            return $"Executing CodeSmell {service.CodeSmell()}";
        }

    }
}